"use strict";

var utils = require("../utils/writer.js");
var Default = require("../service/DefaultService");

var courses = [
  {
    課程編號: 109001,
    課程名稱: "統計學",
    課程簡介: "資料分析的基礎上，研究測定、收集、整理、歸納和分析反映數據資料",
    上課時間: "0800",
    下課時間: "1100",
    課程時長: 3,
    講師編號: 23010115,
  },
  {
    課程編號: 109002,
    課程名稱: "經濟學",
    課程簡介: "透過邏輯推導及分析來認識及瞭解經濟行為與現象",
    上課時間: "1300",
    下課時間: "1500",
    課程時長: 2,
    講師編號: 23010116,
  },
  {
    課程編號: 109003,
    課程名稱: "資料結構",
    課程簡介: "計算機科學中用來儲存、組織和管理數據的方法",
    上課時間: "1500",
    下課時間: "1700",
    課程時長: 2,
    講師編號: 23010117,
  },
];
var teachers = [
  {
    講師編號: 23010115,
    姓名: "周棟祥",
    Email: "johnson.zy.wu@gmail.com",
  },
  {
    講師編號: 23010116,
    姓名: "陳曉明",
    Email: "johnson@gmail.com",
  },
  {
    講師編號: 23010117,
    姓名: "吳志文",
    Email: "ken.wu@gmail.com",
  },
];

//取得課程列表
module.exports.coursesGET = function coursesGET(req, res, next) {
  Default.coursesGET()
    .then(function (response) {
      utils.writeJson(res, courses);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

//取得授課講師列表
module.exports.teachersGET = function teachersGET(req, res, next) {
  Default.teachersGET()
    .then(function (response) {
      utils.writeJson(res, teachers);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

//取得授課講師所開課程列表
module.exports.courseGET = function courseGET(req, res, next) {
  var teacherId = req.query.teacherId;
  var course = courses.filter((course) => course.講師編號 === teacherId);
  if (!course.length) {
    utils.writeJson(res, { message: "找不到課程" }, 404);
    return;
  }
  utils.writeJson(res, course);
};

//新增講師
module.exports.teacherCreate = function teacherCreate(req, res, next) {
  var teacher = req.body;

  // 檢查講師編號是否已存在
  var existingTeacher = teachers.find((t) => t.講師編號 === teacher.講師編號);
  if (existingTeacher) {
    utils.writeJson(res, { message: "講師編號已存在" }, 400);
    return;
  }
  // 檢查資料完整性
  if (
    teacher.講師編號 == null ||
    teacher.姓名 == null ||
    teacher.Email == null
  ) {
    utils.writeJson(res, { message: "缺少教師資訊" }, 400);
    return;
  }
  // 檢查數據格式
  if (
    typeof teacher.講師編號 !== "number" ||
    typeof teacher.姓名 !== "string" ||
    !teacher.Email.includes("@")
  ) {
    utils.writeJson(res, { message: "輸入的數據格式不正確" }, 400);
    return;
  }
  try {
    teachers.push(teacher);
    utils.writeJson(res, { message: "講師新增成功" }, 201);
  } catch (e) {
    utils.writeJson(res, { message: "講師新增失敗" }, 500);
  }
};

//新增課程
module.exports.courseCreate = function courseCreate(req, res, next) {
  var course = req.body;
  try {
    // 檢查課程編號是否已存在
    var existingCourse = courses.find((t) => t.課程編號 === course.課程編號);
    if (existingCourse) {
      utils.writeJson(res, { message: "課程編號已存在" }, 400);
      return;
    }

    // 檢查資料完整性
    if (
      course.課程編號 == null ||
      course.課程名稱 == null ||
      course.課程簡介 == null ||
      course.上課時間 == null ||
      course.下課時間 == null ||
      course.課程時長 == null ||
      course.講師編號 == null
    ) {
      utils.writeJson(res, { message: "缺少課程資訊" }, 400);
      return;
    }
    // 檢查數據格式
    if (
      course.課程編號.toString().length !== 6 ||
      course.講師編號.toString().length !== 8 ||
      typeof course.課程編號 !== "number" ||
      typeof course.課程名稱 !== "string" ||
      typeof course.課程簡介 !== "string" ||
      typeof course.上課時間 !== "string" ||
      typeof course.下課時間 !== "string" ||
      typeof course.課程時長 !== "number" ||
      typeof course.講師編號 !== "number"
    ) {
      utils.writeJson(res, { message: "輸入的數據格式不正確" }, 400);
      return;
    }
    courses.push(course);
    // utils.writeJson(res, course, 201);
    utils.writeJson(res, { message: "課程新增成功" }, 201);
  } catch (e) {
    utils.writeJson(res, { message: "課程新增失敗" }, 404);
  }
};

// 更新課程內容
module.exports.courseUpdate = function courseUpdate(req, res, next) {
  // console.log(req);
  var courseId = req.query.courseId;
  var updatedCourse = req.body;

  var courseIndex = courses.findIndex((course) => {
    return course.課程編號 === courseId;
  });

  if (courseIndex === -1) {
    utils.writeJson(res, { message: "找不到該課程" }, 404);
    return;
  }

  // 檢查課程編號是否已存在，並且不是自己
  const existingCourse = courses.find(
    (course, index) =>
      course.課程編號 === updatedCourse.課程編號 && course.課程編號 !== courseId
  );
  if (existingCourse) {
    utils.writeJson(res, { message: "課程編號已存在" }, 400);
    return;
  }

  courses[courseIndex] = { ...courses[courseIndex], ...updatedCourse };
  // utils.writeJson(res, courses[courseIndex]);
  utils.writeJson(res, { message: "課程更新成功" });
};

// 刪除課程
module.exports.courseDelete = function courseDelete(req, res, next) {
  var courseId = req.query.courseId;

  var courseIndex = courses.findIndex((course) => {
    return course.課程編號 === courseId;
  });

  if (courseIndex === -1) {
    utils.writeJson(res, { message: "找不到該課程" }, 404);
    return;
  }

  courses.splice(courseIndex, 1);
  utils.writeJson(res, { message: "課程刪除成功" });
};
