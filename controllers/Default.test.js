const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../app"); // Assuming this is the file where the routes are defined

chai.use(chaiHttp);
chai.should();

describe("Course API", () => {
  describe("GET /course", () => {
    it("should return the course with the specified teacherId", (done) => {
      const teacherId = 23010115;
      chai
        .request(app)
        .get(`/course?teacherId=${teacherId}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an("array");
          // Add more assertions as needed
          done();
        });
    });

    it("should return 404 if no course is found with the specified teacherId", (done) => {
      const teacherId = 12345678;
      chai
        .request(app)
        .get(`/course?teacherId=${teacherId}`)
        .end((err, res) => {
          res.should.have.status(404);
          // Add more assertions as needed
          done();
        });
    });
  });

  describe("POST /course", () => {
    it("should create a new course", (done) => {
      const course = {
        課程編號: 109004,
        課程名稱: "數學",
        課程簡介: "數字和符號的研究",
        上課時間: "0900",
        下課時間: "1200",
        課程時長: 3,
        講師編號: 23010118,
      };
      chai
        .request(app)
        .post("/course")
        .send(course)
        .end((err, res) => {
          res.should.have.status(201);
          // Add more assertions as needed
          done();
        });
    });

    it("should return 400 if the request body is missing required fields", (done) => {
      const course = {
        // Missing required fields
      };
      chai
        .request(app)
        .post("/course")
        .send(course)
        .end((err, res) => {
          res.should.have.status(400);
          // Add more assertions as needed
          done();
        });
    });

    it("should return 400 if the request body contains invalid data types", (done) => {
      const course = {
        課程編號: "109004", // Invalid data type
        課程名稱: "數學",
        課程簡介: "數字和符號的研究",
        上課時間: "0900",
        下課時間: "1200",
        課程時長: 3,
        講師編號: 23010118,
      };
      chai
        .request(app)
        .post("/course")
        .send(course)
        .end((err, res) => {
          res.should.have.status(400);
          // Add more assertions as needed
          done();
        });
    });
  });

  describe("PUT /course", () => {
    it("should update the course with the specified courseId", (done) => {
      const courseId = 109001;
      const updatedCourse = {
        課程名稱: "統計學（更新）",
      };
      chai
        .request(app)
        .put(`/course?courseId=${courseId}`)
        .send(updatedCourse)
        .end((err, res) => {
          res.should.have.status(200);
          // Add more assertions as needed
          done();
        });
    });

    it("should return 404 if no course is found with the specified courseId", (done) => {
      const courseId = 12345678;
      const updatedCourse = {
        課程名稱: "統計學（更新）",
      };
      chai
        .request(app)
        .put(`/course?courseId=${courseId}`)
        .send(updatedCourse)
        .end((err, res) => {
          res.should.have.status(404);
          // Add more assertions as needed
          done();
        });
    });
  });

  describe("DELETE /course", () => {
    it("should delete the course with the specified courseId", (done) => {
      const courseId = 109001;
      chai
        .request(app)
        .delete(`/course?courseId=${courseId}`)
        .end((err, res) => {
          res.should.have.status(200);
          // Add more assertions as needed
          done();
        });
    });

    it("should return 404 if no course is found with the specified courseId", (done) => {
      const courseId = 12345678;
      chai
        .request(app)
        .delete(`/course?courseId=${courseId}`)
        .end((err, res) => {
          res.should.have.status(404);
          // Add more assertions as needed
          done();
        });
    });
  });
});
