"use strict";

var path = require("path");
var http = require("http");

var oas3Tools = require("oas3-tools");
var PORT = process.env.PORT || 8080;

// swaggerRouter configuration
var options = {
  routing: {
    controllers: path.join(__dirname, "./controllers"),
  },
};

var expressAppConfig = oas3Tools.expressAppConfig(
  path.join(__dirname, "api/openapi.yaml"),
  options
);
var app = expressAppConfig.getApp();

// Initialize the Swagger middleware
var server = http.createServer(app);
server.listen(PORT, function () {
  console.log(
    `Your server is listening on port ${PORT} (http://new-project-6i4r.onrender.com)`
  );
  console.log(
    "Swagger-ui is available on https://new-project-6i4r.onrender.com/docs/"
  );
  console.log(`Swagger-ui is available on http://localhost:${PORT}/docs/`);
});

module.exports = server;
