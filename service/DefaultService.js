'use strict';


/**
 * 建立新課程
 *
 * body Course 新課程資訊
 * no response value expected for this operation
 **/
exports.courseCreate = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * 刪除課程
 *
 * courseId Integer 課程編號
 * no response value expected for this operation
 **/
exports.courseDelete = function(courseId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * 取得授課講師所開課程列表
 *
 * courseId Integer 課程編號
 * returns Course
 **/
exports.courseGET = function(courseId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "課程編號" : 109001,
  "課程簡介" : "資料分析的基礎上，研究測定、收集、整理、歸納和分析反映數據資料",
  "上課時間" : 800,
  "下課時間" : 1100,
  "講師編號" : 23010115,
  "課程名稱" : "統計學",
  "課程時長" : 3
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * 更新課程內容
 *
 * body Course 更新的課程資訊
 * courseId Integer 課程編號
 * no response value expected for this operation
 **/
exports.courseUpdate = function(body,courseId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * 取得課程列表
 *
 * returns List
 **/
exports.coursesGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "課程編號" : 109001,
  "課程簡介" : "資料分析的基礎上，研究測定、收集、整理、歸納和分析反映數據資料",
  "上課時間" : 800,
  "下課時間" : 1100,
  "講師編號" : 23010115,
  "課程名稱" : "統計學",
  "課程時長" : 3
}, {
  "課程編號" : 109001,
  "課程簡介" : "資料分析的基礎上，研究測定、收集、整理、歸納和分析反映數據資料",
  "上課時間" : 800,
  "下課時間" : 1100,
  "講師編號" : 23010115,
  "課程名稱" : "統計學",
  "課程時長" : 3
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * 建立新講師
 *
 * body Teacher 新講師資訊
 * no response value expected for this operation
 **/
exports.teacherCreate = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * 取得授課講師列表
 *
 * returns List
 **/
exports.teachersGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "姓名" : "周棟祥",
  "Email" : "johnson.zy.wu@gmail.com",
  "講師編號" : 23010115
}, {
  "姓名" : "周棟祥",
  "Email" : "johnson.zy.wu@gmail.com",
  "講師編號" : 23010115
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

