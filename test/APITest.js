const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index.js"); // 我的server路徑
chai.should();
chai.use(chaiHttp);

describe("新增講師 API", () => {
  it("正常資料", (done) => {
    const teacher = {
      講師編號: 23010118,
      姓名: "劉展至",
      Email: "jozywu@gmail.com",
    };
    chai
      .request(server)
      .post("/api/createTeacher")
      .send(teacher)
      .end((err, response) => {
        response.should.have.status(201);
        response.body.should.be.a("object");
        done();
      });
  });

  it("講師編號已存在", (done) => {
    const teacher = {
      講師編號: 23010115,
      姓名: "周棟祥",
      Email: "johnson.zy.wu@gmail.com",
    };
    chai
      .request(server)
      .post("/api/createTeacher")
      .send(teacher)
      .end((err, response) => {
        response.should.have.status(400);
        done();
      });
  });

  it("缺少部分資料", (done) => {
    const teacher = {
      姓名: "周棟祥",
      Email: "johnson.zy.wu@gmail.com",
    };
    chai
      .request(server)
      .post("/api/createTeacher")
      .send(teacher)
      .end((err, response) => {
        response.should.have.status(400);
        done();
      });
  });

  it("講師編號為非數字", (done) => {
    const teacher = {
      講師編號: "NotANumber",
      姓名: "周棟祥",
      Email: "johnson.zy.wu@gmail.com",
    };
    chai
      .request(server)
      .post("/api/createTeacher")
      .send(teacher)
      .end((err, response) => {
        response.should.have.status(400);
        done();
      });
  });

  it("姓名為非字串", (done) => {
    const teacher = {
      講師編號: 23010115,
      姓名: 12345,
      Email: "johnson.zy.wu@gmail.com",
    };
    chai
      .request(server)
      .post("/api/createTeacher")
      .send(teacher)
      .end((err, response) => {
        response.should.have.status(400);
        done();
      });
  });

  it("Email不包含@", (done) => {
    const teacher = {
      講師編號: 23010115,
      姓名: "周棟祥",
      Email: "johnson.zy.wu",
    };
    chai
      .request(server)
      .post("/api/createTeacher")
      .send(teacher)
      .end((err, response) => {
        response.should.have.status(400);
        done();
      });
  });
});

describe("新增課程 API", () => {
  it("正常資料", (done) => {
    const course = {
      課程編號: 109004,
      課程名稱: "邏輯思維",
      課程簡介: "邏輯說服力，洞悉本質的過程、非線性思考",
      上課時間: "0900",
      下課時間: "1100",
      課程時長: 2,
      講師編號: 23010115,
    };
    chai
      .request(server)
      .post("/api/createCourse")
      .send(course)
      .end((err, response) => {
        response.should.have.status(201);
        response.body.should.have.property("message").eql("課程新增成功");
        done();
      });
  });

  it("課程編號已存在", (done) => {
    const course = {
      課程編號: 109001,
      課程名稱: "統計學",
      課程簡介:
        "資料分析的基礎上，研究測定、收集、整理、歸納和分析反映數據資料",
      上課時間: "0800",
      下課時間: "1100",
      課程時長: 3,
      講師編號: 23010115,
    };
    chai
      .request(server)
      .post("/api/createCourse")
      .send(course)
      .end((err, response) => {
        response.should.have.status(400);
        done();
      });
  });

  it("缺少部分資料", (done) => {
    const course = {
      課程編號: 109001,
      講師編號: 23010115,
    };
    chai
      .request(server)
      .post("/api/createCourse")
      .send(course)
      .end((err, response) => {
        response.should.have.status(400);
        done();
      });
  });

  it("講師編號或課程編號非數字", (done) => {
    const course = {
      課程編號: "109001",
      課程名稱: "統計學",
      課程簡介:
        "資料分析的基礎上，研究測定、收集、整理、歸納和分析反映數據資料",
      上課時間: "0800",
      下課時間: "1100",
      課程時長: 3,
      講師編號: "23010115",
    };
    chai
      .request(server)
      .post("/api/createCourse")
      .send(course)
      .end((err, response) => {
        response.should.have.status(400);
        done();
      });
  });

  it("講師編號或課程編號不為6,8碼", (done) => {
    const course = {
      課程編號: 1001,
      課程名稱: "統計學",
      課程簡介:
        "資料分析的基礎上，研究測定、收集、整理、歸納和分析反映數據資料",
      上課時間: "0800",
      下課時間: "1100",
      課程時長: 3,
      講師編號: 23010113215,
    };
    chai
      .request(server)
      .post("/api/createCourse")
      .send(course)
      .end((err, response) => {
        response.should.have.status(400);
        done();
      });
  });
});

describe("更新課程 API", () => {
  it("正常資料", (done) => {
    const course = {
      課程編號: 109005,
      課程名稱: "統計學",
      課程簡介:
        "資料分析的基礎上，研究測定、收集、整理、歸納和分析反映數據資料",
      上課時間: "1400",
      下課時間: "1700",
      課程時長: 3,
      講師編號: 23010115,
    };
    chai
      .request(server)
      .put("/api/updateCourse")
      .query({ courseId: 109002 })
      .send(course)
      .end((err, response) => {
        response.should.have.status(200);
        response.body.should.have.property("message").eql("課程更新成功");
        done();
      });
  });

  it("課程編號已存在", (done) => {
    const course = {
      課程編號: 109003,
      課程名稱: "統計學",
      課程簡介:
        "資料分析的基礎上，研究測定、收集、整理、歸納和分析反映數據資料",
      上課時間: "0800",
      下課時間: "1100",
      課程時長: 3,
      講師編號: 23010115,
    };
    chai
      .request(server)
      .put("/api/updateCourse")
      .query({ courseId: 109004 })
      .send(course)
      .end((err, response) => {
        response.should.have.status(400);
        response.body.should.have.property("message").eql("課程編號已存在");
        done();
      });
  });

  it("更新欄位", (done) => {
    const course = {
      課程名稱: "進階計算機概論",
    };
    chai
      .request(server)
      .put("/api/updateCourse")
      .query({ courseId: 109005 })
      .send(course)
      .end((err, response) => {
        response.should.have.status(200);
        response.body.should.have.property("message").eql("課程更新成功");
        done();
      });
  });
});
